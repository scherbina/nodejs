import expressLoader from './express';
import { connectToMongoDB } from './mongodb';

export default async ({expressApp}) => {
    
    await connectToMongoDB();
    console.log('MongoDB is connected and loaded');

    await expressLoader({ app: expressApp});
    console.log('Express loaded');

};