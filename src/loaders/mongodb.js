import mongoose from 'mongoose';
import config from '../config';

export async function connectToMongoDB() {
    const connection = await mongoose
        .connect(
            config.MONGODB_URI, {
            useNewURLParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true
        });
    return connection.connection.db
}