import dotenv from 'dotenv';

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

//Читаємо дані з файла .env в змінну `envFound`
const envFound = dotenv.config({path: '.env.develop'});

if (!envFound) {
	throw new Error(`⚠  Couldn't find .env file  ⚠`);
}

export default {
	/**
	 * @desc Оточення в якому працює система
	 * @type {String}
	 **/
	ENV: process.env.NODE_ENV,

	/**
	 * @desc Порт на якому запускається система
	 * @type {Number}
	 **/
	PORT: process.env.PORT,

	/**
	 * @desc Властивість де зберігається ім'я розробника
	 * @type {String}
	 **/
	DEV_ID: process.env.DEV_NAME,

	/**
	 * @desc Приватний ключ для сервісу
	 **/
	MAILGUN_KEY: process.env.MAILGUN_KEY,

	/**
	 * @desc Домен
	 **/
	MAILGUN_DOMAIN: process.env.MAILGUN_DOMAIN,

	/**
	 * @desc MONGODB
	 **/
	MONGODB_URI: process.env.MONGODB_URI,
	
	/**
	 * @desc JWT_SECRET
	 **/
	JWT_SECRET: process.env.JWT_SECRET
};
