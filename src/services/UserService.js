import { UserModel } from '../db/models';
import jwt from 'jsonwebtoken';
import config from '../config';
import {randomBytes} from 'crypto';
import argon2 from 'argon2';

const token = (user) => {
    const today = new Date();
    const exp = new Date(today);
    exp.setDate(today.getDate() + 60);

    return jwt.sign(
        {
            _id: user._id,
            name: user.name,
            exp: exp.getTime() / 1000,
        },
        config.JWT_SECRET,
    );
}

export default class UserService {
    
    static async getUserById(userId) {
        return UserModel.findById(userId)
    }

    static async login({email, password}) {
        const user = await UserModel.findOne({
            email
        });

        if (!user) {
            throw new Error('User not found');
        }

        const verification = await argon2.verify(user.password, password, {
            salt: user.salt
        });

        if (!verification) {
            throw new Error('Invalid password');
        }

        return {
            token: token(user)
        }
    }

    //Создать пользователя
    static async createUser({name, email, password}) {
        const salt = randomBytes(32);
        const hashedPassword = await argon2.hash(password, {salt});
        const user = await UserModel.create({
            email,
            name,
            password: hashedPassword,
            salt
        });

        // const today = new Date();
		// const exp = new Date(today);
		// exp.setDate(today.getDate() + 60);
        
        // const token = jwt.sign(
		// 	{
		// 		_id: user._id, 
		// 		name: user.name,
		// 		exp: exp.getTime() / 1000,
        //     },
		// 	config.JWT_SECRET,
		// );
        
        const resultUser = user.toObject();
        delete resultUser.__v;
        delete resultUser.salt;
        delete resultUser.password;

        return {
            ...resultUser,
            token: token(user)
        }
    }
}