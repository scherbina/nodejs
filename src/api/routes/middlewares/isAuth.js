import jwt from 'express-jwt';
import config from '../../../config';
const getTokenFromHeader = req => {
    if (
        (
            req.headers.authorization &&
            req.headers.authorization.split(' ')[0] === 'Token') ||
        (
            req.headers.authorization &&
            req.headers.authorization.split(' ')[0] === 'Bearer')
    ) {
        return req.headers.authorization.split(' ')[1];
    }
    return null;
};
const isAuth = jwt({
    secret: config.JWT_SECRET,
    userProperty: 'token',
    algorithms: ['RS256', 'RS384', 'RS512', 'ES256', 'ES384', 'ES512', 'HS256', 'HS384', 'HS512', 'none'],
    getToken: getTokenFromHeader
});
export default isAuth;