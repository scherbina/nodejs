import {Router} from 'express';
import UserService from "../../services/UserService";
import isAuth from './middlewares/isAuth';
// Example -> https://github.com/arb/celebrate
import {Joi, celebrate, CelebrateError} from "celebrate";
const route = Router();
export default function (app) {
	app.use('/auth', route);
	route.get(
		'/get-user/:userId',
		isAuth,
		async (request, response, next) => {
			try {
				const {userId} = request.params;
				const user = await UserService.getUserById(userId);
				return response.json({
					success: true,
					data: user
				})
			} catch (e) {
				next(e);
			}
		}
	);
	route.post('/signin', celebrate({
		body: Joi.object({
			email: Joi
				.string()
				.email()
				.required()
				.error(new Error('Email is required field.')),
			password: Joi
					.string()
					.required()
					.error(new Error('Password is required field.'))
		}),
	}),
		async (request, response, next) => {
			try {
				const {email, password} = request.body;
				const user = await UserService.login({
					email,
					password
				});

				return response.json({
					success: true,
					data: user
				});
			} catch (e) {
				return next(e);
			}
		}
	)
	route.post('/signup',
		(req, res, next) => {
			console.log(req.body);
			next();
		},
		celebrate({
			body: Joi.object({
				name: Joi
					.string()
					.required()
					.error(new Error('Name is required field.')),
				email: Joi
					.string()
					.email()
					.required()
				    .error(new Error('Email is required field.')),
				password: Joi
					.string()
					.required()
					.error(new Error('Password is required field.'))
			})
		}),
		async (request, response, next) => {
			try {
				const {name, email, password} = request.body;
				const user = await UserService.createUser({
					name,
					email,
					password
				});

				return response.json({
					success: true,
					data: user
				});
			} catch (e) {
				return next(e);
			}
		});
}