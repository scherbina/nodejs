import { Router } from 'express';
import homeRoute from './routes/homeRoute';
import productRoute from './routes/productRoute';
import sendGreetingRoute from "./routes/sendGreetingRoute";
import authRoute from './routes/authRoute';
import dashboardRoute from './routes/dashboardRoute';

export default () => {
    const app = Router();
    homeRoute(app);
    productRoute(app);
    sendGreetingRoute(app);
    authRoute(app);
    dashboardRoute(app);
    return app;
}
